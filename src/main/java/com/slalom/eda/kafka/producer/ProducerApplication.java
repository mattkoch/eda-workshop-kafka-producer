package com.slalom.eda.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class ProducerApplication implements CommandLineRunner {

	public static final String KAFKA_TOPIC = "simpleloop";
	public static Logger logger = LoggerFactory.getLogger(ProducerApplication.class);

	@Autowired
	private KafkaTemplate<String, String> template;

	public static void main(String[] args) {
		SpringApplication.run(ProducerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		this.template.send(KAFKA_TOPIC, "foo1");
		this.template.send(KAFKA_TOPIC, "foo2");
		this.template.send(KAFKA_TOPIC, "foo3");
		logger.info("All messages sent");
	}
}
